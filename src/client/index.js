import React from 'react'
import ReactDOM from 'react-dom'

const title = 'My Minimal React Webpack Babel Setup'

ReactDOM.render(
  <div>{title}</div>,
  document.getElementById('app')
)
console.log('My Minimal React Webpack Babel Setup')
module.hot.accept()
